def busqueda_secuencial(arr, val_esp):
    for i in range(len(arr)):
        if arr[i] == val_esp:
            return i
    return -1

def ordenamiento_burbuja(arr):
    n = len(arr)
    for i in range(n):
        for j in range(0, n - i - 1):
            if arr[j] > arr[j + 1]:
                arr[j], arr[j + 1] = arr[j + 1], arr[j]
    return arr

def busqueda_binaria(arr, val_esp):
    bajo = 0
    alto = len(arr) - 1
    while bajo <= alto:
        medio = (bajo + alto) // 2
        if arr[medio] == val_esp:
            return medio
        elif arr[medio] < val_esp:
            bajo = medio + 1
        else:
            alto = medio - 1
    return -1

arr = [64, 34, 25, 12, 22, 11]
val_esp = 11

posicion = busqueda_secuencial(arr, val_esp)
if posicion == -1:
    print(f"El Valor especifico {val_esp} no ha sido encontrado utilizando la busqueda secuencial.")
else:
    print(f"El Valor especifico {val_esp} ha sido encontrado en la posicion {posicion} utilizando la busqueda secuencial.")

arr_ordenado = ordenamiento_burbuja(arr)
print("Arreglo ordenado:", arr_ordenado)

posicion = busqueda_binaria(arr_ordenado, val_esp)
if posicion == -1:
    print(f"El Valor especifico {val_esp} no ha sido encontrado utilizando la busqueda binaria.")
else:
    print(f"El Valor especifico {val_esp} ha sido encontrado en la posicion {posicion} utilizando la busqueda binaria.")


